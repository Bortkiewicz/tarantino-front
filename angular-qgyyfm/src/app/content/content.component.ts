import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
    

function navigation(){
  document.querySelector(".element1").addEventListener("click", () => {
    // Scroll to a certain element
    document.querySelector('.navig1').scrollIntoView({
      behavior: 'smooth'
    });
  });
  
  document.querySelector(".element2").addEventListener("click", () => {
    // Scroll to a certain element
    document.querySelector('.navig2').scrollIntoView({
      behavior: 'smooth'
    });
  });
  
  document.querySelector(".element3").addEventListener("click", () => {
    // Scroll to a certain element
    document.querySelector('.navig3').scrollIntoView({
      behavior: 'smooth'
    });
  });

  document.querySelector(".element4").addEventListener("click", () => {
    // Scroll to a certain element
    document.querySelector('.navig4').scrollIntoView({
      behavior: 'smooth'
    });
  });

  document.querySelector(".element5").addEventListener("click", () => {
    // Scroll to a certain element
    document.querySelector('.navig5').scrollIntoView({
      behavior: 'smooth'
    });
  });

  document.querySelector(".element6").addEventListener("click", () => {
    // Scroll to a certain element
    document.querySelector('.navig6').scrollIntoView({
      behavior: 'smooth'
    });
  });

}

@Component({
  selector: 'content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentComponent implements OnInit {
  
  constructor() { }
  ngOnInit() {
    navigation()
  }

}
