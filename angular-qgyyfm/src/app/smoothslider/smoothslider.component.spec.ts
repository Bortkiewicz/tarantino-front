import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmoothsliderComponent } from './smoothslider.component';

describe('SmoothsliderComponent', () => {
  let component: SmoothsliderComponent;
  let fixture: ComponentFixture<SmoothsliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmoothsliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmoothsliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
