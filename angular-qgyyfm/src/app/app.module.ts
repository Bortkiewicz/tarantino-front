
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatListModule} from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { RouterModule } from '@angular/router';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatChipsModule} from '@angular/material/chips';
import {MatSliderModule} from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';



import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { BillboardComponent } from './billboard/billboard.component';
import { PrzyciskComponent } from './przycisk/przycisk.component';
import { TopbarComponent } from './topbar/topbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component'
import { NavComponent } from './nav/nav.component';
import { ScreenComponent} from './screen/screen.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { GaleriaPionComponent } from './galeriaPion/galeriaPion.component';
import { SmoothsliderComponent } from './smoothslider/smoothslider.component';
import { GridComponent } from './grid/grid.component';
import { RatingComponent } from './rating/rating.component';



@NgModule({
  imports:      [ BrowserModule,
                  FormsModule,
                  MatIconModule,
                  MatMenuModule,
                  BrowserAnimationsModule,
                  MatToolbarModule,
                  MatListModule,
                  MatButtonModule,
                  MatCardModule,
                  MatGridListModule,
                  MatTabsModule,
                  MatButtonToggleModule,
                  MatChipsModule,
                  MatSliderModule,
                  MatFormFieldModule,
                  MatCheckboxModule,
                  
                ],


   declarations: [ 
                  AppComponent,
                  HelloComponent,
                  BillboardComponent,
                  PrzyciskComponent,
                  HeaderComponent,
                  TopbarComponent,
                  NavComponent,
                  FooterComponent,
                  ContentComponent,
                  ScreenComponent,
                  GaleriaComponent,
                  GaleriaPionComponent,
                  SmoothsliderComponent,
                  GridComponent,
                  RatingComponent,
                 ],


  bootstrap:    [ AppComponent ]


})
export class AppModule { }
