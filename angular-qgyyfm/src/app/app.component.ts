import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'my-app',
  templateUrl: './main.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {
  name = 'World';
  ngOnInit(){
    $(document).ready(function(){
    });
  }
}
