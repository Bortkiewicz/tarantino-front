import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';


@Component({
  selector: 'billboard',
  templateUrl: './billboard.component.html',
  styleUrls: ['./billboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BillboardComponent implements OnInit {
  @Input()
  title: string;
  @Input()
  content: string;
  @Input()
  class: string;
  @Input()
  subtitle: string;
  @Input()
  src: string;
  constructor() { }
  ngOnInit() {
  }

}
