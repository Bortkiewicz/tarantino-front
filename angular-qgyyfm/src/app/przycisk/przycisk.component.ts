import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';


@Component({
  selector: 'przycisk',
  templateUrl: './przycisk.component.html',
  styleUrls: ['./przycisk.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrzyciskComponent implements OnInit {
  @Input()
  label: string;
  @Input()
  icon: string;
  constructor() { }
  ngOnInit() {
  }

}
