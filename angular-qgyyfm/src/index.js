// Import stylesheets
import './style.css';

document.querySelector(".top-button").addEventListener("click", () => {
  // Scroll to a certain element
  document.querySelector('.bottom-button').scrollIntoView({
    behavior: 'smooth'
  });
});

document.querySelector(".bottom-button").addEventListener("click", () => {
  // Scroll to a certain element
  document.querySelector('.top-button').scrollIntoView({
    behavior: 'smooth'
  });
});

document.querySelector(".image-button").addEventListener("click", () => {
  // Scroll to a certain element
  document.querySelector('.image').scrollIntoView({
    behavior: 'smooth'
  });
});